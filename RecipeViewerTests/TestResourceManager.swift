//
//  TestResourceManager.swift
//  RecipeViewerTests
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

class TestResourceManager {
    
    /// Test resources error.
    public enum TestResourcesError: Error {
        
        /// File not found.
        case fileNotFound
        
        /// Error while reading data.
        case readError
    }
    
    /// Path for name.
    func path(forName name: String) throws -> String {
        let bundle = Bundle(for: TestResourceManager.self)
        guard let path = bundle.path(forResource: name, ofType: nil) else {
            throw TestResourcesError.fileNotFound
        }
        return path
    }
    
    /// Read data from resource file with name.
    ///
    /// - Parameter name: Read data with given name.
    /// - Returns: Data.
    /// - Throws: `TestResourcesError`.
    public func readData(_ name: String) throws -> Data {
        let filePath = try path(forName: name)
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
            throw TestResourcesError.readError
        }
        return data
    }
    
}
