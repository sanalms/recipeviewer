//
//  RecipeViewerTests.swift
//  RecipeViewerTests
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import XCTest
@testable import RecipeViewer

class ModelTests: XCTestCase {
    
    private enum Constants {
        
        static let popularCount = 3
        static let otherCount = 1
        
        enum Recipe {
            static let title = "Roasted Garlic"
            static let href = "http:\\www.kraftfoods.com\recipes.aspx"
            static let ingredients = "garlic, olive oil"
            static let thumbnail = ""
        }
    }
    
    let resourceManager = TestResourceManager()
    
    func testRecipe() {
        guard let data = try? resourceManager.readData("Recipe.json") else {
            XCTFail("Data Error")
            return
        }
        guard let recipe = try? JSONDecoder().decode(Recipe.self, from: data) else {
            XCTFail("Parsing Error")
            return
        }
        XCTAssert(recipe.title == Constants.Recipe.title)
        XCTAssert(recipe.href == Constants.Recipe.href)
        XCTAssert(recipe.thumbnail == Constants.Recipe.thumbnail)
        XCTAssert(recipe.ingredients == Constants.Recipe.ingredients)
    }
    
    func testRecipeList() {
        guard let data = try? resourceManager.readData("RecipeList.json") else {
            XCTFail("Data Error")
            return
        }
        guard let recipes = try? RecipeParser.parseRecipes(from: data) else {
            XCTFail("Parsing Error")
            return
        }
        XCTAssert(recipes.0.count == Constants.popularCount)
        XCTAssert(recipes.1.count == Constants.otherCount)
        XCTAssertFalse(recipes.0[0].thumbnail.stringified.isEmpty)
        XCTAssertTrue(recipes.1[0].thumbnail.stringified.isEmpty)
    }
    
}
