//
//  Recipe.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation


/// Model to represent recipe.
struct Recipe: Decodable {
    let title: String
    let href: String
    let ingredients: String
    let thumbnail: String?
}
