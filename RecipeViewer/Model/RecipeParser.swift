//
//  RecipeParser.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

final class RecipeParser {
    
    private enum Constants {
        static let results = "results"
    }
    
    /// Parse recipe service response
    ///
    /// - Parameter data: response data
    /// - Returns: Recipe Array
    /// - Throws: json Error
    class func parseRecipes(from data: Data) throws -> ([Recipe], [Recipe]) {
        guard let response = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()),
            let responseDict = response as? JSON,
            let results = responseDict[Constants.results] as? [JSON] else {
                throw ServiceError.jsonError
        }
        let recipes: [Recipe] = results.flatMap {
            if let recipe = try? JSONDecoder().decode(Recipe.self, jsonObject: $0) {
                return recipe
            } else {
                return nil
            }
        }
        let popularRecipes = recipes.filter {
            $0.thumbnail.stringified.isNotEmpty
        }
        let otherRecipes = recipes.filter {
            $0.thumbnail.stringified.isEmpty
        }
        return (popularRecipes, otherRecipes)
    }
}
