//
//  ServiceError.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

/// Different errors may occur during service calls
///
/// - urlError: Invalid url
/// - serverError: Error in server
/// - jsonError: Json parsing error
enum ServiceError: Error {
    case urlError
    case serverError
    case jsonError
    case cachingError
    case downloadError
}
