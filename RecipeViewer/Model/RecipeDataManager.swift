//
//  RecipeDataManager.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

typealias RecipeResponse = Result<([Recipe], [Recipe]), ServiceError>
typealias RecipeCompletion = (RecipeResponse) -> Void

final class RecipeDataManager {
    let service = RecipeService()
    
    
    /// Fetch recipes.
    ///
    /// - Parameter completion: Call back.
    func fetchRecipes(completion: @escaping RecipeCompletion) {
        service.fetchRecipes(completion: completion)
    }
}
