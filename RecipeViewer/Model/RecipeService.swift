//
//  RecipeService.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

typealias RecipeServiceResponse = Result<([Recipe], [Recipe]), ServiceError>
typealias RecipeServiceCompletion = (RecipeServiceResponse) -> Void

final class RecipeService: Service {
    
    
    /// Fetch recipes.
    ///
    /// - Parameter completion: Call back.
    func fetchRecipes(completion: @escaping RecipeServiceCompletion) {
        submitRequest { [weak self] result in
            guard let this = self else { return }
            switch result {
            case .success(let data):
                guard let recipes = try? RecipeParser.parseRecipes(from: data) else {
                    this.complete(with: completion, result: .failure(.jsonError))
                    return
                }
                this.complete(with: completion, result: .success(recipes))
            case .failure(let error):
                this.complete(with: completion, result: .failure(error))
            }
        }
    }
    
    private func complete(with completion: @escaping RecipeServiceCompletion,
                          result: RecipeServiceResponse) {
        DispatchQueue.main.async {
            completion(result)
        }
    }
}
