//
//  Service.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

typealias JSON = [String : Any]
typealias ServiceResult = Result<Data, ServiceError>
typealias ServiceCompletion = (ServiceResult) -> Void

enum HTTPMethod: String {
    case get
    case post
}


/// Base service class
/// Implement common functionalities
class Service {
    
    private enum Constants {
        static let baseUrl = "https://g525204.github.io/recipes.json"
    }
    
    /// Base url
    var baseUrl: String = Constants.baseUrl
    let urlSession: URLSession
    
    
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    /// Submit a request to server and d
    ///
    /// - Parameters:
    ///   - endPoint: Request end point
    ///   - method: http method
    ///   - queryParameters: Request query parametrs
    ///   - body: Request Body
    ///   - headers: Request headers
    ///   - completion: Service completion handler
    func submitRequest(with endPoint: String = "",
                       method: HTTPMethod = .get,
                       queryParameters: JSON? = nil,
                       body: Data? = nil,
                       headers: [String: String]? = nil,
                       completion: @escaping ServiceCompletion) {
        var urlString = "\(baseUrl)\(endPoint)"
        if case method = HTTPMethod.get {
            urlString = "\(urlString)\(parameterEndPoint(with: queryParameters))"
        }
        guard let url = URL(string: urlString) else {
            completion(.failure(.urlError))
            return
        }
        var request = URLRequest(url: url)
        if let body = body,
            method == .post {
            request.httpBody = body
            request.httpMethod = HTTPMethod.post.rawValue
        }
        if let headers = headers, method == .post {
            request.allHTTPHeaderFields = headers
        }
        let dataRequest = urlSession.dataTask(with: request) { data, response, error in
            guard error == nil, let data = data else {
                completion(.failure(.serverError))
                return
            }
            completion(.success(data))
        }
        dataRequest.resume()
    }
    
    
    /// Create endpoint string from paramaters JSON
    ///
    /// - Parameter parameters: JSON
    /// - Returns: end point string
    private func parameterEndPoint(with parameters: JSON?) -> String {
        guard let parameters = parameters as? [String: String] else {
            return ""
        }
        let str = parameters.reduce("") {
            return "\($0)&\($1.0)=\($1.1)"
        }
        return "?\(str.dropFirst())"
    }
}
