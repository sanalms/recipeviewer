//
//  Strings.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 28/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

enum Strings {
    
    static let okText = NSLocalizedString(
        "ok.message",
        tableName: "RecipeViewer",
        value: "Ok",
        comment: "ok")
    
    static let webErrorMessage = NSLocalizedString(
        "web.error.message",
        tableName: "RecipeViewer",
        value: "Failed to load page",
        comment: "fail")
    
    static let serviceErrorMessage = NSLocalizedString(
        "service.error.message",
        tableName: "RecipeViewer",
        value: "Failed to fetch recipes",
        comment: "fail")
    
    static let listTitle = NSLocalizedString(
        "list.titile",
        tableName: "RecipeViewer",
        value: "Recipes",
        comment: "titile")
    
    static let detailTitle = NSLocalizedString(
        "detail.titile",
        tableName: "RecipeViewer",
        value: "Recipe Details",
        comment: "detail titile")
    
    static let webTitle = NSLocalizedString(
        "web.titile",
        tableName: "RecipeViewer",
        value: "Details",
        comment: "titile")
}
