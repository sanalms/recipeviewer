//
//  ImageDownloader.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 28/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

typealias ImageDownloadResult = Result<UIImage?, ServiceError>
typealias ImageDownloadResponse = (ImageDownloadResult) -> Void

final class ImageDownloader {
    private var downloadTask: URLSessionDownloadTask? = nil
    
    
    /// Download Image if not exists in cache folder
    ///
    /// - Parameters:
    ///   - urlString: Image url
    ///   - completion: download completion
    func downloadImage(urlString: String, completion: @escaping ImageDownloadResponse) {
        let fileName = urlString.md5
        
        if let path = fileUrl(for: fileName)?.path, FileManager.default.fileExists(atPath: path) {
            completion(.success(UIImage(contentsOfFile: path)))
            return
        }
        guard let url = URL(string: urlString) else {
            completion(.failure(.urlError))
            return
        }
        downloadTask = URLSession.shared.downloadTask(with: url) { [weak self] locationUrl , response, error in
            guard let this = self else { return }
            guard let locationUrl = locationUrl, let destinationFileUrl = this.fileUrl(for: fileName) else {
                DispatchQueue.main.async {
                    completion(.failure(.cachingError))
                }
                return
            }
            do {
                try FileManager.default.copyItem(at: locationUrl, to: destinationFileUrl)
                DispatchQueue.main.async {
                    completion(.success(UIImage(contentsOfFile: destinationFileUrl.path)))
                }
            } catch {
                DispatchQueue.main.async {
                    // Handling the scenario, If another cell download same image same time
                    completion(.success(UIImage(contentsOfFile: locationUrl.path)))
                }
            }
        }
        downloadTask?.resume()
    }
    
    private func fileUrl(for fileName: String) -> URL? {
        guard let documentsUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return nil
        }
        return documentsUrl.appendingPathComponent(fileName)
    }
    
    deinit {
        //Cancel the download task
        downloadTask?.cancel()
        downloadTask = nil
    }
}
