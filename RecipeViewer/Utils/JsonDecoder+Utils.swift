//
//  JsonDecoder+Utils.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

extension JSONDecoder {
    
    /// Decodes a top-level value of the given type from the given JSON representation.
    ///
    /// - parameter type: The type of the value to decode.
    /// - parameter json: The json object to decode from.
    /// - returns: A value of the requested type.
    /// - throws: `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid JSON.
    /// - throws: An error if any value throws an error during decoding.
    func decode<T>(_ type: T.Type, jsonObject: JSON) throws -> T where T: Decodable {
        let data = try JSONSerialization.data(withJSONObject: jsonObject, options: [])
        return try decode(type, from: data)
    }
}
