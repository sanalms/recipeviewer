//
//  String+Utils.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import Foundation

public protocol OptionalString {}
extension String: OptionalString {}

public extension String {
    
    /// Check whether string is not empty.
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
    
    /// md5 hash of string.
    var md5:String {
        get{
            let messageData = self.data(using:.utf8)!
            var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
            
            _ = digestData.withUnsafeMutableBytes {digestBytes in
                messageData.withUnsafeBytes {messageBytes in
                    CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
                }
            }
            
            return digestData.map { String(format: "%02hhx", $0) }.joined()
        }
    }
}

public extension Optional where Wrapped: OptionalString {
    /// If the string is `nil` returns an empty one, otherwise `self`.
    var stringified: String {
        return (self as? String) ?? ""
    }
}
