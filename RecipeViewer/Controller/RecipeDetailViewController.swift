//
//  RecipeDetailViewController.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

final class RecipeDetailViewController: UIViewController {
    
    private enum Constants {
        static let urlSegue = "showWebView"
        static let imageName = "placeHolder"
    }
    
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var ingredientsLabel: UILabel!
    @IBOutlet private var linkLabel: UILabel!
    
    private var imageDownloader: ImageDownloader?
    
    var recipe: Recipe!

    override func viewDidLoad() {
        super.viewDidLoad()
        updateDetails()
    }
    
    private func updateDetails() {
        title = Strings.detailTitle
        titleLabel.text = recipe.title
        ingredientsLabel.text = recipe.ingredients
        imageView.image = UIImage(named: Constants.imageName)
        linkLabel.text = recipe.href
        if let thumbnail = recipe.thumbnail {
            imageDownloader = ImageDownloader()
            imageDownloader?.downloadImage(urlString: thumbnail) {[weak self] result in
                guard let this = self else { return }
                if case .success(let image) = result {
                    this.imageView.image = image
                }
            }
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.urlSegue,
            let navigationVC = segue.destination as? UINavigationController,
            let webVC = navigationVC.topViewController as? RecipeWebViewController {
            webVC.urlString = recipe.href
        }
    }
}
