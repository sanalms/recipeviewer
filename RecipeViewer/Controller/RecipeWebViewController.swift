//
//  RecipeWebViewController.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 28/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit
import WebKit

final class RecipeWebViewController: UIViewController {

    @IBOutlet private var webView: WKWebView!
    @IBOutlet private var errorLabel: UILabel!
    
    var urlString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Strings.webTitle
        if let url = URL(string: urlString) {
            errorLabel.isHidden = true
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        } else {
            errorLabel.isHidden = false
            errorLabel.text = Strings.webErrorMessage
        }
    }
    
    @IBAction private func cancelTap(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
