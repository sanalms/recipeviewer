//
//  RecipeCollectionViewController.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

final class RecipeCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private enum Constants {
        static let edge: CGFloat = 3.0
        static let reuseIdentifier = "ImageCellIdentifier"
        static let headerIdentifier = "HeaderIdentifier"
        static let labelCellIdentifier = "LabelCellIdentifier"
        static let showDetailSegue = "showDetail"
        static let sectionCount = 2
        static let labelCellHeight: CGFloat = 70
    }

    let dataManager = RecipeDataManager()
    let sectionHeaders = ["Popular", "Other"]
    
    private var activityView: UIActivityIndicatorView?
    private var currentSelectedRecipe: Recipe?
    
    private var popularRecipes = [Recipe]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    private var otherRecipes = [Recipe]() {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    private var imageCellSize: CGSize {
        if traitCollection.horizontalSizeClass == .regular {
            let width = view.bounds.width / 3 - 3 * Constants.edge
            let height = width * 0.8
            return CGSize(width: width, height: height)
        } else {
            let width = view.bounds.width / 2 - 2 * Constants.edge
            let height = width * 0.8
            return CGSize(width: width, height: height)
        }
    }
    
    private var labelCellSize: CGSize {
        let width = view.bounds.width - 2 * Constants.edge
        return CGSize(width: width, height: Constants.labelCellHeight)
        
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Strings.listTitle
        fetchRecipes()
    }
    
    // MARK: - Private functions
    
    private func fetchRecipes() {
        showActivity()
        dataManager.fetchRecipes { [weak self] result in
            guard let this = self else { return }
            this.hideActivity()
            switch result {
            case .success(let recipes):
                this.popularRecipes = recipes.0
                this.otherRecipes = recipes.1
            case .failure:
                this.showError()
            }
        }
    }
    
    private func showError() {
        let alert = UIAlertController(
            title: Strings.serviceErrorMessage,
            message: nil,
            preferredStyle: .alert)
        let okAction = UIAlertAction(title: Strings.okText, style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func showActivity() {
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        guard let activityView = activityView, let collectionView = collectionView else {
            return
        }
        activityView.center = collectionView.center
        activityView.startAnimating()
        collectionView.addSubview(activityView)
    }
    
    private func hideActivity() {
        guard let activityView = activityView else {
            return
        }
        activityView.stopAnimating()
        activityView.removeFromSuperview()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.showDetailSegue,
            let detailVC = segue.destination as? RecipeDetailViewController,
            let recipe = currentSelectedRecipe {
            detailVC.recipe = recipe
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Constants.sectionCount
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return popularRecipes.count
        case 1:
            return otherRecipes.count
        default:
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 ,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseIdentifier, for: indexPath) as? RecipeImageCollectionViewCell {
            cell.configure(with: popularRecipes[indexPath.row])
            return cell
        }else if indexPath.section == 1 ,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.labelCellIdentifier, for: indexPath) as? LabelCollectionViewCell {
            cell.configure(with: popularRecipes[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constants.headerIdentifier, for: indexPath) as? SectionHeader{
            sectionHeader.configure(with: sectionHeaders[indexPath.section])
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    // MARK: - Collection flow layout
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        if indexPath.section == 0 {
            size = imageCellSize
        } else {
            size = labelCellSize
        }
        return size
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            currentSelectedRecipe = popularRecipes[indexPath.row]
        } else {
            currentSelectedRecipe = otherRecipes[indexPath.row]
        }
        performSegue(withIdentifier: Constants.showDetailSegue, sender: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func refreshButtonTap(_ sender: Any) {
        fetchRecipes()
    }
}
