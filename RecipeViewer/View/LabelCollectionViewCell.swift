//
//  LabelCollectionViewCell.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 28/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

final class LabelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var textLabel: UILabel!
    
    
    /// Cofigure the cell.
    ///
    /// - Parameter recipe: Recipe
    func configure(with recipe: Recipe) {
        textLabel.text = recipe.title
    }
    
}
