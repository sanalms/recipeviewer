//
//  SectionHeader.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

final class SectionHeader: UICollectionReusableView {
    @IBOutlet private var sectionHeaderLabel: UILabel!
    
    
    /// Cofigure the cell.
    ///
    /// - Parameter name: String
    func configure(with name: String) {
        sectionHeaderLabel.text = name
    }
}
