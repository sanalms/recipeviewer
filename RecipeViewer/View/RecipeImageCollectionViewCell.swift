//
//  RecipeImageCollectionViewCell.swift
//  RecipeViewer
//
//  Created by SOMAN, SANAL M on 27/06/18.
//  Copyright © 2018 Sanal MS. All rights reserved.
//

import UIKit

final class RecipeImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var textLabel: UILabel!
    
    private var imageDownloader: ImageDownloader?
    private var activityView: UIActivityIndicatorView?
    
    
    /// Configure the cell.
    ///
    /// - Parameter recipe: Recipe
    func configure(with recipe: Recipe) {
        textLabel.text = recipe.title
        
        guard let thumbnail = recipe.thumbnail else {
            imageView.isHidden = true
            return
        }
        imageDownloader = nil
        imageDownloader = ImageDownloader()
        imageView.image = nil
        imageView.isHidden = false
        showActivity()
        imageDownloader?.downloadImage(urlString: thumbnail) {[weak self] result in
            guard let this = self else { return }
            this.hideActivity()
            if case .success(let image) = result {
                this.imageView.image = image
            }
        }
    }
    
    private func showActivity() {
        activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        guard let activityView = activityView else {
            return
        }
        activityView.center = self.center
        activityView.startAnimating()
        self.addSubview(activityView)
        activityView.bringSubview(toFront: imageView)
    }
    
    private func hideActivity() {
        guard let activityView = activityView else {
            return
        }
        activityView.stopAnimating()
        activityView.removeFromSuperview()
    }
}
